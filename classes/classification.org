#+title: Classification

#+NAME: imports classification
#+BEGIN_SRC python :session data :results replace drawer output :exports both :noweb yes :eval never-export
from typing import Any, List, Tuple, Union, Dict
from matplotlib.axes import Axes
from matplotlib.figure import Figure
import matplotlib.pyplot as plt
import numbers
import pandas as pd
import numpy as np
from tabulate import tabulate
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LogisticRegression
import statsmodels.api as sm
from sklearn.neighbors import KNeighborsClassifier
from sklearn.svm import SVC
from sklearn.tree import DecisionTreeClassifier
from sklearn.multiclass import OneVsRestClassifier
import matplotlib
matplotlib.use('TKAgg')


def print_tabulate(df: pd.DataFrame):
    print(tabulate(df, headers=df.columns, tablefmt="orgtbl"), flush=True)

def transform_variable(df: pd.DataFrame, x:str)->pd.DataFrame:
    if isinstance(df[x][0], numbers.Number):
        return df[x]
    else:
        return pd.DataFrame({x: [i for i in range(0, len(df[x]))]})

def split_train_test(df: pd.DataFrame, x_label:str, y_label:str, test_size:float=0.3, random_state:int=0):
    fixed_x = transform_variable(df, x_label)
    return train_test_split(fixed_x, df[y_label], test_size=test_size, random_state = random_state)


def split_train_test_classification(df: pd.DataFrame, x_labels:List[str], y_label:str, test_size:float=0.3, random_state:int=0):
    fixed_x = df[x_labels]
    return train_test_split(fixed_x, df[y_label], test_size=test_size, random_state = random_state)


def train_model(x,y,
                model_class: Union[type[KNeighborsClassifier], type[LogisticRegression], type[SVC]],
                model_params: Dict[str, Any]) \
        -> Union[KNeighborsClassifier, LogisticRegression, SVC]:
    print(f"using {model_class.__name__} with {model_params}")
    # print(f"params? {model_params is None or len(model_params)==0}")
    model = model_class() if model_params is None or len(model_params) == 0 else model_class(**model_params)
    trained_model = model.fit(x,y)
    return trained_model


def create_plot(subplots:int=111)-> Tuple[Figure, Axes]:
    fig = plt.figure()
    ax = fig.add_subplot(subplots)
    return fig, ax


def plot_points(ax:Axes, x,y, scatter_params:Dict[str,Any]):
    return ax.scatter(x,y, **scatter_params)


def plot_line(ax:Axes, x,y, color, label='line'):
    return ax.plot(x,y, color=color, label=label)


def finalize_and_save(fig:Figure, ax:Axes, file_name:str, ylim=None, rotation:int=90, size_inches:Tuple[int,int]=(27,18)):
    ax.tick_params(axis='x', labelrotation=rotation)
    fig.set_size_inches(*size_inches)
    # axis.set_xlim([min(fixed_x), max(fixed_x)])
    if ylim is not None:
        ax.set_ylim(ylim)
    fig.savefig(file_name)
    plt.close()

def prepare_file()-> pd.DataFrame:
    df = pd.read_csv("csv/typed_uanl.csv") # type: pd.DataFrame
    # print_tabulate(df.head(10))
    df_by_sal = df.groupby(["Tipo", "Fecha"])[["Sueldo Neto"]].agg({'Sueldo Neto': ['sum', 'count', 'mean']}) #type: pd.DataFrame
    df_by_sal.reset_index(inplace=True)
    df_by_sal.columns = ['Tipo','Fecha', 'sueldo_neto_sum', 'conteo_empleados', 'promedio_pago']
    # df_by_sal["sueldo_mensual"] = df_by_sal["sueldo_mensual"]**10
    # df_by_sal.reset_index(inplace=True)
    return df_by_sal


def detect_outlier(df: pd.DataFrame, x:str, y:str)-> pd.DataFrame:
    fixed_x = transform_variable(df, x)
    model= sm.OLS(df[y],sm.add_constant(fixed_x)).fit()
    bands = pd.read_html(model.summary().tables[1].as_html(),header=0,index_col=0)[0]
    coef = pd.read_html(model.summary().tables[1].as_html(),header=0,index_col=0)[0]['coef']
    lr_dict = {'m': coef.values[1], 'b': coef.values[0], 'low_band': bands['[0.025'].iloc[0], 'hi_band': bands['0.975]'].iloc[0]}
    def outlier_dist(y:float, x:float, lr_dict:Dict[str,float])-> int:
        # predict = 1 if y > (lr_dict['m']*x+1.2*lr_dict['hi_band']) or y < (lr_dict['m']*x+0.8*lr_dict['low_band']) else 0
        # print(f"y:{y} yhat_hi:{lr_dict['m']*x+lr_dict['hi_band']} yhat_lo:{lr_dict['m']*x+lr_dict['low_band']} predict: {predict}")
        return 1 if y > (lr_dict['m']*x+(1*lr_dict['hi_band'])) or y < (lr_dict['m']*x+(1*lr_dict['low_band'])) else 0

    outlier_df = pd.DataFrame({'outliers': [outlier_dist(row[y],row[x], lr_dict) for _, row in fixed_x.join(df[y]).iterrows()]})
    return df.join(outlier_df)


def prepare_reg_file()-> pd.DataFrame:
    df = pd.read_csv("csv/typed_uanl.csv") # type: pd.DataFrame
    # print_tabulate(df.head(10))
    df_by_sal = df.groupby(["Fecha"])[["Sueldo Neto"]].agg({'Sueldo Neto': ['sum']}) #type: pd.DataFrame
    df_by_sal.reset_index(inplace=True)
    df_by_sal.columns = ['Fecha', 'sueldo_neto_sum']
    # df_by_sal["sueldo_mensual"] = df_by_sal["sueldo_mensual"]**10
    df_by_sal.reset_index(inplace=True)
    return df_by_sal

#+END_SRC



#+BEGIN_SRC python :session data :results replace drawer output :exports both :tangle uanl_classification_knn_model.py :noweb yes :eval never-export
<<imports classification>>

def knn_classification_model(df: pd.DataFrame, x:List[str], y: str, df_predict:pd.DataFrame, n_neighbors:int)->None:
    features = df[x]
    trained_model = train_model(features, df[y], KNeighborsClassifier, {'n_neighbors':n_neighbors}) # type: KNeighborsClassifier
    df_predict[y]=trained_model.predict(df_predict)

    fig, ax = create_plot()
    categories = np.unique(df[y])
    for index, class_ in enumerate(categories):
        filter_df = df[df[y]==class_]
        filter_predict_df = df_predict[df_predict[y]==class_]
        filter_features = [filter_df[label] for label in x]
        filter_predict = [filter_predict_df[label] for label in x]
        plot_points(ax, *filter_features, scatter_params={'marker':'.', 'c':f"C{index}", 'label':class_})
        if len(filter_predict) > 0:
            plot_points(ax, *filter_predict, scatter_params={'marker':'x', 'c':f"C{index}", 'label':class_})

    ax.legend(loc="upper left")
    ax.set_xlabel(x[0])
    ax.set_ylabel(x[1])
    ax.set_title(f"knn_classifier")
    finalize_and_save(fig, ax, f'img/knn_classifier_{y}_{"_".join(x)}.png', size_inches=(11,6), rotation=0)

df=prepare_file()
df_predict = pd.DataFrame([{"promedio_pago":8000, "conteo_empleados":2400},
                           {"promedio_pago":8000, "conteo_empleados":400},
                           {"promedio_pago":13000, "conteo_empleados":800},
                           {"promedio_pago":15000, "conteo_empleados":3500},
                           {"promedio_pago":15000, "conteo_empleados":4400},
                           {"promedio_pago":18000, "conteo_empleados":2000}])

knn_classification_model(df, ["promedio_pago", "conteo_empleados"], "Tipo", df_predict, 5)
#+END_SRC

#+RESULTS:
:results:
using KNeighborsClassifier with {'n_neighbors': 5}
:end:


#+attr_html: :width 600px
#+attr_latex: :width 800px
[[file:img/knn_classifier_Tipo_promedio_pago_conteo_empleados.png]]

#+BEGIN_SRC python :session data :results replace drawer output :exports both :tangle uanl_classification_knn_model_predict.py :noweb yes :eval never-export
<<imports classification>>

def knn_classification_model_predict(df: pd.DataFrame, x:List[str], y: str, n_neighbors:int, test_size:int=0.3)->None:
    X_train, X_test, y_train, y_test = split_train_test_classification(df,x,y,test_size=test_size, random_state = 0)
    trained_model = train_model(X_train, y_train, KNeighborsClassifier, {'n_neighbors':n_neighbors}) # type: KNeighborsClassifier
    score_test = trained_model.score(X_test, y_test)
    score_train = trained_model.score(X_train, y_train)
    predict = trained_model.predict(X_test)
    print(f"scores:[train {score_train:.4}, test {score_test:.4}]")

    fig, ax = create_plot()
    categories = np.unique(df[y])
    for index, class_ in enumerate(categories):
        filter_train_df = X_train[y_train==class_]
        filter_test_df_correct = X_test[(y_test==class_) & (predict==class_)]
        filter_test_df_failure = X_test[(y_test!=class_) & (predict==class_)]
        # filter_predict_df = df_predict[df_predict[y]==class_]
        # filter_features = [filter_df[label] for label in x]
        # filter_predict = [filter_predict_df[label] for label in x]


        plot_points(ax, filter_train_df[x[0]], filter_train_df[x[1]], scatter_params={'marker':'.', 'c':f"C{index}", 'label':class_})
        if len(filter_test_df_correct) > 0:
            plot_points(ax, filter_test_df_correct[x[0]], filter_test_df_correct[x[1]], scatter_params={'marker':'v', 'c':f"C{index}", 'label':class_})
        if len(filter_test_df_failure) > 0:
            plot_points(ax, filter_test_df_failure[x[0]], filter_test_df_failure[x[1]], scatter_params={'marker':'x', 'c':f"C{index}", 'label':class_})

    ax.legend(loc="upper left")
    ax.set_xlabel(x[0])
    ax.set_ylabel(x[1])
    ax.set_title(f"KNN Classifier scores:[train {score_train:.4}, test {score_test:.4}]")
    finalize_and_save(fig, ax, f'img/knn_classifier_model_{y}_{"_".join(x)}.png', size_inches=(11,6), rotation=0)

df=prepare_file()
knn_classification_model_predict(df, ["promedio_pago", "conteo_empleados"], "Tipo", 5, 0.3)
#+END_SRC

#+RESULTS:
:results:
using KNeighborsClassifier with {'n_neighbors': 5}
scores:[train 0.8955, test 0.908]
:end:


#+attr_html: :width 600px
#+attr_latex: :width 800px
[[file:img/knn_classifier_model_Tipo_promedio_pago_conteo_empleados.png]]
#+NAME: logistic_regression
#+BEGIN_SRC python :session data :results replace drawer output :exports both :tangle uanl_classification_logistic_regression_model.py :noweb yes :eval never-export
<<imports classification>>

def logistic_regression_model(df: pd.DataFrame, x:List[str], y: str)->None:
    df_fixed = df.copy()
    df_fixed['Fecha'] = transform_variable(df,"Fecha")
    X_train, X_test, y_train, y_test = split_train_test_classification(df_fixed,x,y,test_size=0.3, random_state = 0)
    trained_model = train_model(X_train, y_train, LogisticRegression, {'random_state':0, 'C':1})
    score_train = trained_model.score(X_train, y_train)
    score_test = trained_model.score(X_test, y_test)
    predict = trained_model.predict(X_test)
    predict2 = trained_model.predict(X_train)
    # print(predict2)

    fig, ax = create_plot()
    categories = np.unique(df[y])
    for index, class_ in enumerate(categories):
        filter_train_df = X_train[y_train==class_]
        filter_test_df_correct = X_test[(y_test==class_) & (predict==class_)]
        filter_test_df_failure = X_test[(y_test!=class_) & (predict==class_)]

        plot_points(ax, filter_train_df[x[0]], filter_train_df[x[1]], scatter_params={'marker':'.', 'c':f"C{index}", 'label':class_})
        if len(filter_test_df_correct) > 0:
            plot_points(ax, filter_test_df_correct[x[0]], filter_test_df_correct[x[1]], scatter_params={'marker':'v', 'c':f"C{index}", 'label':class_})
        if len(filter_test_df_failure) > 0:
            plot_points(ax, filter_test_df_failure[x[0]], filter_test_df_failure[x[1]], scatter_params={'marker':'x', 'c':f"C{index}", 'label':class_})

    ax.legend(loc="upper left")
    ax.set_xlabel(x[0])
    ax.set_ylabel(x[1])
    ax.set_title(f"lr_classifier scores:[train: {score_train:.4}, test: {score_test:.4}]")
    finalize_and_save(fig, ax, f'img/lr_classifier_model_{y}_{"_".join(x)}.png', size_inches=(11,6), rotation=0)

lr_df = detect_outlier(prepare_reg_file(), "Fecha", "sueldo_neto_sum")
logistic_regression_model(lr_df, ["Fecha", "sueldo_neto_sum"], "outliers" )
#+END_SRC

#+RESULTS: logistic_regression
:results:
/tmp/babel-SlY2Y1/python-qGyjhW:86: FutureWarning: Passing literal html to 'read_html' is deprecated and will be removed in a future version. To read from a literal string, wrap it in a 'StringIO' object.
  bands = pd.read_html(model.summary().tables[1].as_html(),header=0,index_col=0)[0]
/tmp/babel-SlY2Y1/python-qGyjhW:87: FutureWarning: Passing literal html to 'read_html' is deprecated and will be removed in a future version. To read from a literal string, wrap it in a 'StringIO' object.
  coef = pd.read_html(model.summary().tables[1].as_html(),header=0,index_col=0)[0]['coef']
using LogisticRegression with {'random_state': 0, 'C': 10000000000000}
:end:


#+attr_html: :width 600px
#+attr_latex: :width 800px
[[file:img/lr_classifier_model_outliers_Fecha_sueldo_neto_sum.png]]

#+BEGIN_SRC python :session data :results replace drawer output :exports both :tangle uanl_multi_classification_logistic_regression_model.py :noweb yes :eval never-export
<<imports logistic_regression>>
df=prepare_file()
df_lr_tipo = df.copy()
df_lr_tipo["FACULTAD"] = df_lr_tipo["Tipo"].apply(lambda x: 1 if x =="FACULTAD" else 0)
df_lr_tipo["ADMIN"] = df_lr_tipo["Tipo"].apply(lambda x: 1 if x =="ADMIN" else 0)

logistic_regression_model(df_lr_tipo, ["promedio_pago", "conteo_empleados"], "FACULTAD" )
logistic_regression_model(df_lr_tipo, ["promedio_pago", "conteo_empleados"], "ADMIN" )
#+END_SRC

#+RESULTS:
:results:
using LogisticRegression with {'random_state': 0, 'C': 1}
using LogisticRegression with {'random_state': 0, 'C': 1}
:end:


# #+attr_html: :width 600px
# #+attr_latex: :width 800px
[[file:img/lr_classifier_model_FACULTAD_promedio_pago_conteo_empleados.png]]
# #+attr_html: :width 600px
# #+attr_latex: :width 800px
[[file:img/lr_classifier_model_ADMIN_promedio_pago_conteo_empleados.png]]

#+BEGIN_SRC python :session data :results replace drawer output :exports both :tangle uanl_multi_classification_logistic_regression_model.py :noweb yes :eval never-export
<<imports classification>>

def multi_class_logistic_regression_model(df: pd.DataFrame, x:List[str], y: str)->None:
    df_fixed = df.copy()
    df_fixed['Fecha'] = transform_variable(df,"Fecha")
    X_train, X_test, y_train, y_test = split_train_test_classification(df_fixed,x,y,test_size=0.3, random_state = 0)
    trained_model = train_model(X_train, y_train, OneVsRestClassifier, {'estimator':LogisticRegression(random_state=0, C=1)})
    score_train = trained_model.score(X_train, y_train)
    score_test = trained_model.score(X_test, y_test)
    predict = trained_model.predict(X_test)
    predict2 = trained_model.predict(X_train)
    # print(predict2)

    fig, ax = create_plot()
    categories = np.unique(df[y])
    for index, class_ in enumerate(categories):
        filter_train_df = X_train[y_train==class_]
        filter_test_df_correct = X_test[(y_test==class_) & (predict==class_)]
        filter_test_df_failure = X_test[(y_test!=class_) & (predict==class_)]

        plot_points(ax, filter_train_df[x[0]], filter_train_df[x[1]], scatter_params={'marker':'.', 'c':f"C{index}", 'label':class_})
        if len(filter_test_df_correct) > 0:
            plot_points(ax, filter_test_df_correct[x[0]], filter_test_df_correct[x[1]], scatter_params={'marker':'v', 'c':f"C{index}", 'label':class_})
        if len(filter_test_df_failure) > 0:
            plot_points(ax, filter_test_df_failure[x[0]], filter_test_df_failure[x[1]], scatter_params={'marker':'x', 'c':f"C{index}", 'label':class_})

    ax.legend(loc="upper left")
    ax.set_xlabel(x[0])
    ax.set_ylabel(x[1])
    ax.set_title(f"lr_classifier scores:[train: {score_train:.4}, test: {score_test:.4}]")
    finalize_and_save(fig, ax, f'img/mc_lr_classifier_model_{y}_{"_".join(x)}.png', size_inches=(11,6), rotation=0)

df=prepare_file()
multi_class_logistic_regression_model(df, ["promedio_pago", "conteo_empleados"], "Tipo")
#+END_SRC

#+RESULTS:
:results:
using OneVsRestClassifier with {'estimator': LogisticRegression(C=1, random_state=0)}
:end:


# #+attr_html: :width 600px
# #+attr_latex: :width 800px
[[file:img/mc_lr_classifier_model_Tipo_promedio_pago_conteo_empleados.png]]

#+NAME:SVM
#+BEGIN_SRC python :session data :results replace drawer output :exports both :tangle uanl_svm_classification_model.py :noweb yes :eval never-export
<<imports classification>>
def SVM_model(df: pd.DataFrame, x:List[str], y: str)->None:
    df_fixed = df.copy()
    df_fixed['Fecha'] = transform_variable(df,"Fecha")
    X_train, X_test, y_train, y_test = split_train_test_classification(df_fixed,x,y,test_size=0.3, random_state = 0)
    trained_model = train_model(X_train, y_train, SVC, {'random_state':0, 'C':1})
    score_train = trained_model.score(X_train, y_train)
    score_test = trained_model.score(X_test, y_test)
    predict = trained_model.predict(X_test)
    predict2 = trained_model.predict(X_train)
    # print(predict2)

    fig, ax = create_plot()
    categories = np.unique(df[y])
    for index, class_ in enumerate(categories):
        filter_train_df = X_train[y_train==class_]
        filter_test_df_correct = X_test[(y_test==class_) & (predict==class_)]
        filter_test_df_failure = X_test[(y_test!=class_) & (predict==class_)]

        plot_points(ax, filter_train_df[x[0]], filter_train_df[x[1]], scatter_params={'marker':'.', 'c':f"C{index}", 'label':class_})
        if len(filter_test_df_correct) > 0:
            plot_points(ax, filter_test_df_correct[x[0]], filter_test_df_correct[x[1]], scatter_params={'marker':'v', 'c':f"C{index}", 'label':class_})
        if len(filter_test_df_failure) > 0:
            plot_points(ax, filter_test_df_failure[x[0]], filter_test_df_failure[x[1]], scatter_params={'marker':'x', 'c':f"C{index}", 'label':class_})

    ax.legend(loc="upper left")
    ax.set_xlabel(x[0])
    ax.set_ylabel(x[1])
    ax.set_title(f"svm_classifier scores:[train: {score_train:.4}, test: {score_test:.4}]")
    finalize_and_save(fig, ax, f'img/svc_classifier_model_{y}_{"_".join(x)}.png', size_inches=(11,6), rotation=0)

lr_df = detect_outlier(prepare_reg_file(), "Fecha", "sueldo_neto_sum")
SVM_model(lr_df, ["Fecha", "sueldo_neto_sum"], "outliers" )
#+END_SRC

#+RESULTS: SVM
:results:
/tmp/babel-bOy1XM/python-Y1oOA9:87: FutureWarning: Passing literal html to 'read_html' is deprecated and will be removed in a future version. To read from a literal string, wrap it in a 'StringIO' object.
  bands = pd.read_html(model.summary().tables[1].as_html(),header=0,index_col=0)[0]
/tmp/babel-bOy1XM/python-Y1oOA9:88: FutureWarning: Passing literal html to 'read_html' is deprecated and will be removed in a future version. To read from a literal string, wrap it in a 'StringIO' object.
  coef = pd.read_html(model.summary().tables[1].as_html(),header=0,index_col=0)[0]['coef']
using SVC with {'random_state': 0, 'C': 1}
:end:


# #+attr_html: :width 600px
# #+attr_latex: :width 800px
[[file:img/svc_classifier_model_outliers_Fecha_sueldo_neto_sum.png]]

#+BEGIN_SRC python :session data :results replace drawer output :exports both :tangle uanl_multi_classification_logistic_regression_model.py :noweb yes :eval never-export
<<imports SVM>>

df=prepare_file()
df_lr_tipo = df.copy()
df_lr_tipo["FACULTAD"] = df_lr_tipo["Tipo"].apply(lambda x: 1 if x =="FACULTAD" else 0)
df_lr_tipo["ADMIN"] = df_lr_tipo["Tipo"].apply(lambda x: 1 if x =="ADMIN" else 0)
SVM_model(df_lr_tipo, ["promedio_pago", "conteo_empleados"], "FACULTAD" )
SVM_model(df_lr_tipo, ["promedio_pago", "conteo_empleados"], "ADMIN" )
#+END_SRC

#+RESULTS:
:results:
using SVC with {'random_state': 0, 'C': 1}
using SVC with {'random_state': 0, 'C': 1}
:end:


# #+attr_html: :width 600px
# #+attr_latex: :width 800px
[[file:img/svc_classifier_model_FACULTAD_promedio_pago_conteo_empleados.png]]
# #+attr_html: :width 600px
# #+attr_latex: :width 800px
[[file:img/svc_classifier_model_ADMIN_promedio_pago_conteo_empleados.png]]

#+BEGIN_SRC python :session data :results replace drawer output :exports both :tangle uanl_multi_classification_svm_model.py :noweb yes :eval never-export
<<imports classification>>

def multi_class_SVM_model(df: pd.DataFrame, x:List[str], y: str)->None:
    df_fixed = df.copy()
    df_fixed['Fecha'] = transform_variable(df,"Fecha")
    X_train, X_test, y_train, y_test = split_train_test_classification(df_fixed,x,y,test_size=0.3, random_state = 0)
    trained_model = train_model(X_train, y_train, OneVsRestClassifier, {'estimator':SVC(random_state=0,C=1)})
    score_train = trained_model.score(X_train, y_train)
    score_test = trained_model.score(X_test, y_test)
    predict = trained_model.predict(X_test)
    predict2 = trained_model.predict(X_train)

    fig, ax = create_plot()
    categories = np.unique(df[y])
    for index, class_ in enumerate(categories):
        filter_train_df = X_train[y_train==class_]
        filter_test_df_correct = X_test[(y_test==class_) & (predict==class_)]
        filter_test_df_failure = X_test[(y_test!=class_) & (predict==class_)]

        plot_points(ax, filter_train_df[x[0]], filter_train_df[x[1]], scatter_params={'marker':'.', 'c':f"C{index}", 'label':class_})
        if len(filter_test_df_correct) > 0:
            plot_points(ax, filter_test_df_correct[x[0]], filter_test_df_correct[x[1]], scatter_params={'marker':'v', 'c':f"C{index}", 'label':class_})
        if len(filter_test_df_failure) > 0:
            plot_points(ax, filter_test_df_failure[x[0]], filter_test_df_failure[x[1]], scatter_params={'marker':'x', 'c':f"C{index}", 'label':class_})

    ax.legend(loc="upper left")
    ax.set_xlabel(x[0])
    ax.set_ylabel(x[1])
    ax.set_title(f"mc_svm_classifier scores:[train: {score_train:.4}, test: {score_test:.4}]")
    finalize_and_save(fig, ax, f'img/mc_svc_classifier_model_{y}_{"_".join(x)}.png', size_inches=(11,6), rotation=0)

df=prepare_file()
multi_class_SVM_model(df, ["promedio_pago", "conteo_empleados"], "Tipo")
#+END_SRC

#+RESULTS:
:results:
using OneVsRestClassifier with {'estimator': SVC(C=1, random_state=0)}
:end:

#+attr_html: :width 600px
#+attr_latex: :width 800px
[[file:img/mc_svc_classifier_model_Tipo_promedio_pago_conteo_empleados.png]]

#+NAME:Decision tree
#+BEGIN_SRC python :session data :results replace drawer output :exports both :tangle uanl_random_forrest_classification_model.py :noweb yes :eval never-export
<<imports classification>>
from sklearn.tree import plot_tree
def decision_tree_model(df: pd.DataFrame, x:List[str], y: str)->None:
    df_fixed = df.copy()
    df_fixed['Fecha'] = transform_variable(df,"Fecha")
    X_train, X_test, y_train, y_test = split_train_test_classification(df_fixed,x,y,test_size=0.3, random_state = 0)
    trained_model = train_model(X_train, y_train, DecisionTreeClassifier, {'random_state':0, 'max_depth': 30, 'min_samples_leaf': 5})
    score_train = trained_model.score(X_train, y_train)
    score_test = trained_model.score(X_test, y_test)
    predict = trained_model.predict(X_test)
    predict2 = trained_model.predict(X_train)
    # print(predict2)

    fig, ax = create_plot()
    categories = np.unique(df[y])
    for index, class_ in enumerate(categories):
        filter_train_df = X_train[y_train==class_]
        filter_test_df_correct = X_test[(y_test==class_) & (predict==class_)]
        filter_test_df_failure = X_test[(y_test!=class_) & (predict==class_)]

        plot_points(ax, filter_train_df[x[0]], filter_train_df[x[1]], scatter_params={'marker':'.', 'c':f"C{index}", 'label':class_})
        if len(filter_test_df_correct) > 0:
            plot_points(ax, filter_test_df_correct[x[0]], filter_test_df_correct[x[1]], scatter_params={'marker':'v', 'c':f"C{index}", 'label':class_})
        if len(filter_test_df_failure) > 0:
            plot_points(ax, filter_test_df_failure[x[0]], filter_test_df_failure[x[1]], scatter_params={'marker':'x', 'c':f"C{index}", 'label':class_})

    ax.legend(loc="upper left")
    ax.set_xlabel(x[0])
    ax.set_ylabel(x[1])
    ax.set_title(f"decision tree classifier scores:[train: {score_train:.4}, test: {score_test:.4}]")
    finalize_and_save(fig, ax, f'img/decision_tree_classifier_model_{y}_{"_".join(x)}.png', size_inches=(11,6), rotation=0)
    plot_tree(trained_model)
    plt.savefig(f"img/decision_tree_{y}_{"_".join(x)}.png",bbox_inches = "tight")

lr_df = detect_outlier(prepare_reg_file(), "Fecha", "sueldo_neto_sum")
decision_tree_model(lr_df, ["Fecha", "sueldo_neto_sum"], "outliers" )
#+END_SRC

#+RESULTS: Decision tree
:results:
/tmp/babel-bOy1XM/python-m0KH6F:87: FutureWarning: Passing literal html to 'read_html' is deprecated and will be removed in a future version. To read from a literal string, wrap it in a 'StringIO' object.
  bands = pd.read_html(model.summary().tables[1].as_html(),header=0,index_col=0)[0]
/tmp/babel-bOy1XM/python-m0KH6F:88: FutureWarning: Passing literal html to 'read_html' is deprecated and will be removed in a future version. To read from a literal string, wrap it in a 'StringIO' object.
  coef = pd.read_html(model.summary().tables[1].as_html(),header=0,index_col=0)[0]['coef']
using DecisionTreeClassifier with {'random_state': 0, 'max_depth': 30, 'min_samples_leaf': 5}
:end:


#+attr_html: :width 600px
#+attr_latex: :width 800px
[[file:img/decision_tree_classifier_model_outliers_Fecha_sueldo_neto_sum.png]]

#+attr_html: :width 600px
#+attr_latex: :width 800px
[[file:img/decision_tree_outliers_Fecha_sueldo_neto_sum.png]]
#+BEGIN_SRC python :session data :results replace drawer output :exports both :tangle uanl_multi_classification_logistic_regression_model.py :noweb yes :eval never-export
<<imports SVM>>

df=prepare_file()
df_lr_tipo = df.copy()
df_lr_tipo["FACULTAD"] = df_lr_tipo["Tipo"].apply(lambda x: 1 if x =="FACULTAD" else 0)
df_lr_tipo["ADMIN"] = df_lr_tipo["Tipo"].apply(lambda x: 1 if x =="ADMIN" else 0)
decision_tree_model(df_lr_tipo, ["promedio_pago", "conteo_empleados"], "FACULTAD" )
decision_tree_model(df_lr_tipo, ["promedio_pago", "conteo_empleados"], "ADMIN" )
#+END_SRC

#+RESULTS:
:results:
using DecisionTreeClassifier with {'random_state': 0, 'max_depth': 30, 'min_samples_leaf': 5}
using DecisionTreeClassifier with {'random_state': 0, 'max_depth': 30, 'min_samples_leaf': 5}
:end:


#+attr_html: :width 600px
#+attr_latex: :width 800px
[[file:img/decision_tree_classifier_model_FACULTAD_promedio_pago_conteo_empleados.png]]
#+attr_html: :width 600px
#+attr_latex: :width 800px
[[file:img/decision_tree_FACULTAD_promedio_pago_conteo_empleados.png]]
#+attr_html: :width 600px
#+attr_latex: :width 800px
[[file:img/decision_tree_classifier_model_ADMIN_promedio_pago_conteo_empleados.png]]
#+attr_html: :width 600px
#+attr_latex: :width 800px
[[file:img/decision_tree_ADMIN_promedio_pago_conteo_empleados.png]]

#+BEGIN_SRC python :session data :results replace drawer output :exports both :tangle uanl_multi_classification_svm_model.py :noweb yes :eval never-export
<<imports classification>>

def multi_class_decision_tree_model(df: pd.DataFrame, x:List[str], y: str)->None:
    df_fixed = df.copy()
    df_fixed['Fecha'] = transform_variable(df,"Fecha")
    X_train, X_test, y_train, y_test = split_train_test_classification(df_fixed,x,y,test_size=0.3, random_state = 0)
    trained_model = train_model(X_train, y_train, OneVsRestClassifier, {'estimator':DecisionTreeClassifier(random_state=0, max_depth= 30, min_samples_leaf=5)})
    score_train = trained_model.score(X_train, y_train)
    score_test = trained_model.score(X_test, y_test)
    predict = trained_model.predict(X_test)
    predict2 = trained_model.predict(X_train)

    fig, ax = create_plot()
    categories = np.unique(df[y])
    for index, class_ in enumerate(categories):
        filter_train_df = X_train[y_train==class_]
        filter_test_df_correct = X_test[(y_test==class_) & (predict==class_)]
        filter_test_df_failure = X_test[(y_test!=class_) & (predict==class_)]

        plot_points(ax, filter_train_df[x[0]], filter_train_df[x[1]], scatter_params={'marker':'.', 'c':f"C{index}", 'label':class_})
        if len(filter_test_df_correct) > 0:
            plot_points(ax, filter_test_df_correct[x[0]], filter_test_df_correct[x[1]], scatter_params={'marker':'v', 'c':f"C{index}", 'label':class_})
        if len(filter_test_df_failure) > 0:
            plot_points(ax, filter_test_df_failure[x[0]], filter_test_df_failure[x[1]], scatter_params={'marker':'x', 'c':f"C{index}", 'label':class_})

    ax.legend(loc="upper left")
    ax.set_xlabel(x[0])
    ax.set_ylabel(x[1])
    ax.set_title(f"mc decision tree_classifier scores:[train: {score_train:.4}, test: {score_test:.4}]")
    finalize_and_save(fig, ax, f'img/mc_decision_tree_classifier_model_{y}_{"_".join(x)}.png', size_inches=(11,6), rotation=0)

df=prepare_file()
multi_class_decision_tree_model(df, ["promedio_pago", "conteo_empleados"], "Tipo")
#+END_SRC

#+RESULTS:
:results:
using OneVsRestClassifier with {'estimator': DecisionTreeClassifier(max_depth=30, min_samples_leaf=5, random_state=0)}
:end:

#+attr_html: :width 600px
#+attr_latex: :width 800px
        [[file:img/mc_decision_tree_classifier_model_Tipo_promedio_pago_conteo_empleados.png]]
