#+title: Model_evaluation
#+STARTUP: entitiespretty latexpreview
#+LATEX_HEADER: \usepackage{tikz}
* Model Evaluation
For model Evaluation is commonly used accuracy as evaluation metric for our models.

* Confusion Matrix
|                    | Positive Condition  | Negative Condition   |
|--------------------+---------------------+----------------------|
| Predicted Positive | True positive (TP)  | False positive (FP)  |
| Predicted Negative | False Negative (FN) | True Negative   (TN) |

#+BEGIN_SRC python :session data :results replace drawer output :exports both :noweb yes :eval never-export
from sklearn.metrics import confusion_matrix, ConfusionMatrixDisplay
import matplotlib.pyplot as plt
import matplotlib
matplotlib.use('TKAgg')

y_true = [0, 1, 0, 1, 0, 1, 1, 0]
y_pred = [1, 1, 0, 1, 0, 0, 1, 0]
cm = confusion_matrix(y_true, y_pred)
print(cm)
disp = ConfusionMatrixDisplay(confusion_matrix=cm)
disp.plot()
plt.savefig(f"img/confusion_matrix_sample.png")
#+end_src

#+RESULTS:
:results:
[[3 1]
 [1 3]]
:end:

[[file:img/confusion_matrix_sample.png]]
* Accuracy

$Accuracy = \frac{TP+TN}{TP+FN+FP+TN}$
has problems with imbalanced datasets

#+BEGIN_SRC python :session data :results replace drawer output :exports both :noweb yes :eval never-export
from sklearn.metrics import accuracy_score

y_true = [0, 1, 0, 1, 0, 1, 1, 0]
y_pred = [1, 1, 0, 1, 0, 0, 1, 0]

accuracy = accuracy_score(y_true, y_pred)

print("Accuracy Score:", accuracy)
#+end_src

#+RESULTS:
:results:
Accuracy Score: 0.75
:end:

* Precision
also known as positive predictive value

$Precision = \frac{TP}{TP+FP}$
#+BEGIN_SRC python :session data :results replace drawer output :exports both :noweb yes :eval never-export
from sklearn.metrics import precision_score

y_true = [1, 0, 1, 1, 0, 1, 0, 0, 1, 0, 0, 1, 1, 1, 1, 0, 1]
y_pred = [1, 0, 1, 0, 0, 1, 1, 0, 1, 1, 1, 0, 1, 0, 0, 1, 1]

precision = precision_score(y_true, y_pred)

print("Precision Score:", precision)
#+end_src

#+RESULTS:
:results:
Precision Score: 0.6
:end:

* Recall
also known as sensitivity
$Recall = \frac{TP}{TP+FN}$
#+BEGIN_SRC python :session data :results replace drawer output :exports both :noweb yes :eval never-export
from sklearn.metrics import recall_score

y_true = [1, 0, 1, 1, 0, 1, 0, 0, 1, 0, 0, 1, 1, 1, 1, 0, 1]
y_pred = [1, 0, 1, 0, 0, 1, 1, 0, 1, 1, 1, 0, 1, 0, 0, 1, 1]

recall = recall_score(y_true, y_pred)

print("Recall Score:", recall)

#+end_src

#+RESULTS:
:results:
Recall Score: 0.6
:end:

* F1
Harmonic mean of precision and recall
$F1 =2 \times \frac{Precision \times Recall}{Precision + Recall}$
#+BEGIN_SRC python :session data :results replace drawer output :exports both :noweb yes :eval never-export
from sklearn.metrics import f1_score

# Example data
y_true = [1, 0, 1, 1, 0, 1, 0, 0, 1, 0, 0, 1, 1, 1, 1, 0, 1]
y_pred = [1, 0, 1, 1, 0, 1, 0, 0, 1, 1, 0, 1, 1, 0, 1, 0, 1]

# Calculate F1 score
f1 = f1_score(y_true, y_pred)

# Print the F1 score
print("F1 Score:", f1)
#+end_src
* ROC-AUC
ROC (Receiver Operating Characteristic) curve and AUC (Area Under the Curve)
#+BEGIN_SRC python :session data :results replace drawer output :exports both :noweb yes :eval never-export
from sklearn.metrics import roc_curve, roc_auc_score
import matplotlib.pyplot as plt
import matplotlib
matplotlib.use('TKAgg')
# Example data
y_true = [0, 1, 0, 1, 0, 1, 1, 0]
y_scores = [0.6, 0.8, 0.3, 0.9, 0.2, 0.7, 0.5, 0.4]

# Calculate ROC curve
fpr, tpr, _ = roc_curve(y_true, y_scores)

# Calculate ROC-AUC score
roc_auc = roc_auc_score(y_true, y_scores)

# Print ROC-AUC Score
print('ROC-AUC Score:', roc_auc)

# Plot ROC curve
plt.figure()
plt.plot(fpr, tpr, color='darkorange', lw=2, label=f'ROC curve (area = {roc_auc:0.2f})')
plt.plot([0, 1], [0, 1], color='navy', lw=2, linestyle='--')
plt.xlim([-0.01, 1.0])
plt.ylim([-0.01, 1.01])
plt.xlabel('False Positive Rate')
plt.ylabel('True Positive Rate')
plt.title('Receiver Operating Characteristic (ROC) Curve')
plt.legend(loc="lower right")
plt.savefig(f"img/roc_auc_sample.png")
#+end_src

#+RESULTS:
: None

[[file:img/roc_auc_sample.png]]
* PR Curve
Precision-Recall Area Under the Curve
#+BEGIN_SRC python :session data :results replace drawer output :exports both :noweb yes :eval never-export
from sklearn.metrics import precision_recall_curve, auc
import matplotlib.pyplot as plt
import matplotlib
matplotlib.use('TKAgg')

# Example data
y_true = [0, 1, 0, 1, 0, 1, 1, 0]
y_scores = [0.6, 0.8, 0.3, 0.9, 0.2, 0.7, 0.5, 0.4]

# Calculate precision-recall curve
precision, recall, _ = precision_recall_curve(y_true, y_scores)

# Calculate PR-AUC score
pr_auc = auc(recall, precision)

# Print PR-AUC Score
print('PR-AUC Score:', pr_auc)

# Plot Precision-Recall curve
plt.figure()
plt.plot(recall, precision, color='blue', lw=2, label=f'PR curve (area = {pr_auc:0.2f})')
plt.xlabel('Recall')
plt.ylabel('Precision')
plt.title('Precision-Recall Curve')
plt.legend(loc="lower left")
plt.savefig(f"img/pr_auc_sample.png")

#+end_src

#+RESULTS:
:results:
PR-AUC Score: 0.94375
:end:

[[file:img/pr_auc_sample.png]]
* Cross Validation
Splits the data in n parts and then trains a model were each part is used as test data while the others are used as train data.
#+begin_src latex :exports results :results raw graphic file :file img/cross_validation.png :imagemagick yes :headers '("\\usepackage{tikz}") :fit no :cache yes :eval never-export
\usetikzlibrary{shapes}
\tikzset{pblock/.style = {rectangle split, rectangle split horizontal,
                      rectangle split parts=5, very thick,draw=black!50, top
                      color=white,bottom color=black!20, align=center}}

\begin{tikzpicture}
  \node[pblock]{\nodepart[text width=1cm]{one} Train
                \nodepart{two}Test
                \nodepart{three}Test
                \nodepart{four}Test
                \nodepart{five}Test
              };
\end{tikzpicture}

\begin{tikzpicture}
  \node[pblock]{\nodepart[text width=1cm]{one} Test
                \nodepart{two}Train
                \nodepart{three}Test
                \nodepart{four}Test
                \nodepart{five}Test
              };
\end{tikzpicture}

\begin{tikzpicture}
  \node[pblock]{\nodepart[text width=1cm]{one} Test
                \nodepart{two}Test
                \nodepart{three}Train
                \nodepart{four}Test
                \nodepart{five}Test
              };
\end{tikzpicture}

\begin{tikzpicture}
  \node[pblock]{\nodepart[text width=1cm]{one} Test
                \nodepart{two}Test
                \nodepart{three}Test
                \nodepart{four}Train
                \nodepart{five}Test
              };
\end{tikzpicture}

\begin{tikzpicture}
  \node[pblock]{\nodepart[text width=1cm]{one} Test
                \nodepart{two}Test
                \nodepart{three}Test
                \nodepart{four}Test
                \nodepart{five}Train
              };
\end{tikzpicture}
% \tikzset{/tikz/pblock/.append style = {every one node part/.style={text width=1cm}}}
% \begin{tikzpicture}
%   \node[pblock]{\nodepart{one} Some first text
%                 \nodepart{two}Some other text};
\end{tikzpicture}
#+end_src

#+RESULTS[2881811a9eabc3da884619e1b11eac56951e0daf]:
[[file:img/cross_validation.png]]
* GridSearchCV
#+BEGIN_SRC python :session data :results replace drawer output :exports both :tangle uanl_model_cv.py :noweb yes :eval never-export
import numbers
import pandas as pd
import statsmodels.api as sm
from typing import Any, List, Tuple, Union, Dict, Callable
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LogisticRegression
from sklearn.neighbors import KNeighborsClassifier
from sklearn.svm import SVC
from sklearn.tree import DecisionTreeClassifier
from sklearn.multiclass import OneVsRestClassifier
from sklearn.model_selection import GridSearchCV
from sklearn.metrics import roc_auc_score, precision_score, recall_score
from sklearn.dummy import DummyClassifier
from tabulate import tabulate
import matplotlib
matplotlib.use('TKAgg')

import warnings
warnings.filterwarnings("ignore")

def print_tabulate(df: pd.DataFrame):
    print(tabulate(df, headers=df.columns, tablefmt="orgtbl"), flush=True)


def transform_variable(df: pd.DataFrame, x_list:List[str])->pd.DataFrame:
    return pd.DataFrame({x: df[x] if isinstance(df[x][0], numbers.Number) else [i for i in range(0, len(df[x]))] for x in x_list })


def split_train_test_classification(df: pd.DataFrame, x_labels:List[str], y_label:str, test_size:float=0.3, random_state:int=0):
    fixed_x = transform_variable(df, x_labels)
    return train_test_split(fixed_x, df[y_label], test_size=test_size, random_state = random_state)


def prepare_file()-> pd.DataFrame:
    df = pd.read_csv("csv/typed_uanl.csv") # type: pd.DataFrame
    # print_tabulate(df.head(10))
    df_by_sal = df.groupby(["Tipo", "Fecha"])[["Sueldo Neto"]].agg({'Sueldo Neto': ['sum', 'count', 'mean']}) #type: pd.DataFrame
    df_by_sal.reset_index(inplace=True)
    df_by_sal.columns = ['Tipo','Fecha', 'sueldo_neto_sum', 'conteo_empleados', 'promedio_pago']
    # df_by_sal["sueldo_mensual"] = df_by_sal["sueldo_mensual"]**10
    # df_by_sal.reset_index(inplace=True)
    return df_by_sal


def detect_outlier(df: pd.DataFrame, x:str, y:str)-> pd.DataFrame:
    fixed_x = transform_variable(df, [x])
    model= sm.OLS(df[y],sm.add_constant(fixed_x)).fit()
    bands = pd.read_html(model.summary().tables[1].as_html(),header=0,index_col=0)[0]
    coef = pd.read_html(model.summary().tables[1].as_html(),header=0,index_col=0)[0]['coef']
    lr_dict = {'m': coef.values[1], 'b': coef.values[0], 'low_band': bands['[0.025'].iloc[0], 'hi_band': bands['0.975]'].iloc[0]}
    def outlier_dist(y:float, x:float, lr_dict:Dict[str,float])-> int:
        # predict = 1 if y > (lr_dict['m']*x+1.2*lr_dict['hi_band']) or y < (lr_dict['m']*x+0.8*lr_dict['low_band']) else 0
        # print(f"y:{y} yhat_hi:{lr_dict['m']*x+lr_dict['hi_band']} yhat_lo:{lr_dict['m']*x+lr_dict['low_band']} predict: {predict}")
        return 1 if y > (lr_dict['m']*x+(1*lr_dict['hi_band'])) or y < (lr_dict['m']*x+(1*lr_dict['low_band'])) else 0

    outlier_df = pd.DataFrame({'outliers': [outlier_dist(row[y],row[x], lr_dict) for _, row in fixed_x.join(df[y]).iterrows()]})
    return df.join(outlier_df)

def prepare_reg_file()-> pd.DataFrame:
    df = pd.read_csv("csv/typed_uanl.csv") # type: pd.DataFrame
    # print_tabulate(df.head(10))
    df_by_sal = df.groupby(["Fecha"])[["Sueldo Neto"]].agg({'Sueldo Neto': ['sum']}) #type: pd.DataFrame
    df_by_sal.reset_index(inplace=True)
    df_by_sal.columns = ['Fecha', 'sueldo_neto_sum']
    # df_by_sal["sueldo_mensual"] = df_by_sal["sueldo_mensual"]**10
    df_by_sal.reset_index(inplace=True)
    return df_by_sal


def grid_search_model(df: pd.DataFrame, x:List[str], y: str, model_to_use, parameters_grid: Dict[str, Any], score_name: str, cross_validation_parts: int, validation_size:float=0.3)->None:
    X_train, X_validation, y_train, y_validation = split_train_test_classification(df,x,y,test_size=validation_size, random_state = 0)

    grid_search = GridSearchCV(estimator=model_to_use, param_grid=parameters_grid, cv=cross_validation_parts, scoring=score_name, n_jobs=-1)
    grid_search.fit(X_train, y_train)
    print("Best parameters: {0}".format(grid_search.best_params_))
    best_classifier = grid_search.best_estimator_ # get trained model with best parameters
    y_validation_prediction = best_classifier.predict(X_validation)
    scorer_fn = grid_search.scorer_ # type: _BaseScorer
    validation_score = scorer_fn(best_classifier, X_validation, y_validation)
    print(f"{score_name} scores [validation: {validation_score:.4}, train {grid_search.best_score_} ]")
    print_tabulate(pd.DataFrame(grid_search.cv_results_))

score_fn_name='f1'
parameters_grid = {
    # 'C': [0.01, 0.1, 1, 100, 500],
    'max_depth': [1,2,3,4,5]
}
# parameters_grid = {
#     'n_neighbors':[2,3,4]
# }
# model_to_use = KNeighborsClassifier()
model_to_use =  DecisionTreeClassifier(random_state=0, min_samples_leaf= 5) #LogisticRegression(random_state=0)
model_to_use_2 = DummyClassifier(random_state=0, strategy='constant', constant=1)
raw_df=prepare_reg_file()
# print_tabulate(raw_df.head(5))
lr_df = detect_outlier(raw_df, "Fecha", "sueldo_neto_sum")
# print_tabulate(lr_df.head(5))
grid_search_model(lr_df, ["Fecha", "sueldo_neto_sum"], "outliers", model_to_use, parameters_grid, score_fn_name, 5, 0.2)
grid_search_model(lr_df, ["Fecha", "sueldo_neto_sum"], "outliers", model_to_use_2, {}, score_fn_name, 5, 0.2)
#+end_src

#+RESULTS:
:results:
Best parameters: {'max_depth': 3}
f1 scores [validation: 0.75, train 0.3704761904761905 ]
|    |   mean_fit_time |   std_fit_time |   mean_score_time |   std_score_time |   param_max_depth | params           |   split0_test_score |   split1_test_score |   split2_test_score |   split3_test_score |   split4_test_score |   mean_test_score |   std_test_score |   rank_test_score |
|----+-----------------+----------------+-------------------+------------------+-------------------+------------------+---------------------+---------------------+---------------------+---------------------+---------------------+-------------------+------------------+-------------------|
|  0 |      0.00247664 |    0.000259215 |        0.00391145 |      0.00061963  |                 1 | {'max_depth': 1} |            0        |                 0   |            0        |                 0   |            0.333333 |         0.0666667 |         0.133333 |                 5 |
|  1 |      0.00331054 |    8.56591e-05 |        0.00444422 |      0.000483806 |                 2 | {'max_depth': 2} |            0.5      |                 0.5 |            0        |                 0.4 |            0.4      |         0.36      |         0.185472 |                 4 |
|  2 |      0.00434971 |    0.0013543   |        0.00484514 |      0.000592442 |                 3 | {'max_depth': 3} |            0.666667 |                 0   |            0.285714 |                 0.4 |            0.5      |         0.370476  |         0.223524 |                 1 |
|  3 |      0.00542936 |    0.00215862  |        0.00439277 |      0.000161155 |                 4 | {'max_depth': 4} |            0.666667 |                 0   |            0.285714 |                 0.4 |            0.5      |         0.370476  |         0.223524 |                 1 |
|  4 |      0.00418601 |    0.000683865 |        0.00373273 |      0.000473475 |                 5 | {'max_depth': 5} |            0.666667 |                 0   |            0.285714 |                 0.4 |            0.5      |         0.370476  |         0.223524 |                 1 |
Best parameters: {}
f1 scores [validation: 0.6667, train 0.5672727272727273 ]
|    |   mean_fit_time |   std_fit_time |   mean_score_time |   std_score_time | params   |   split0_test_score |   split1_test_score |   split2_test_score |   split3_test_score |   split4_test_score |   mean_test_score |   std_test_score |   rank_test_score |
|----+-----------------+----------------+-------------------+------------------+----------+---------------------+---------------------+---------------------+---------------------+---------------------+-------------------+------------------+-------------------|
|  0 |     0.000942707 |    0.000101898 |        0.00279851 |      0.000368905 | {}       |            0.545455 |            0.545455 |            0.545455 |                 0.6 |                 0.6 |          0.567273 |        0.0267217 |                 1 |
:end:
