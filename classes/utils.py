"""
Módulo de utilerías para operaciones con DataFrames de pandas.
"""

from typing import Dict, Tuple, Union, Any
from tabulate import tabulate

from matplotlib.figure import Figure
from matplotlib.axes import Axes

from sklearn.model_selection import train_test_split
from sklearn.linear_model import LinearRegression, Ridge, Lasso
from sklearn.neighbors import KNeighborsRegressor

import pandas as pd
import matplotlib.pyplot as plt

def imprimir_tabla(df: pd.DataFrame) -> None:
    """
    Imprime un DataFrame en formato tabular.

    Args:
    - df (pd.DataFrame): El DataFrame a imprimir.
    """
    print(tabulate(df, headers=df.columns, tablefmt="orgtbl"), flush=True)

def transformar_variable(df: pd.DataFrame, columna: str) -> pd.Series:
    """
    Transforma una columna del DataFrame en una serie numérica si no lo es.

    Args:
    - df (pd.DataFrame): El DataFrame.
    - columna (str): El nombre de la columna a transformar.

    Returns:
    - pd.Series: La columna transformada.
    """
    if pd.api.types.is_numeric_dtype(df[columna]):
        return df[columna]
    return pd.DataFrame(range(len(df[columna])))

def dividir_datos(x: pd.DataFrame, y: pd.Series,
    tamano_prueba: float = 0.3, estado_aleatorio: int = 0
) -> Tuple[pd.DataFrame, pd.DataFrame, pd.Series, pd.Series]:
    """
    Dividir los datos en conjuntos de entrenamiento y prueba.

    Args:
    - X (pd.DataFrame): DataFrame con las características de entrada.
    - y (pd.Series): Serie con la variable objetivo.
    - tamano_prueba (float): Proporción del conjunto de datos que se utilizará
    como prueba.
    - estado_aleatorio (int): Semilla para aleatoriedad.

    Returns:
    - Tuple[pd.DataFrame, pd.DataFrame, pd.Series, pd.Series]: Conjuntos de
    entrenamiento y prueba para características y objetivo.
    """
    return train_test_split(x, y, test_size=tamano_prueba,
        random_state=estado_aleatorio)

def entrenar_modelo(x, y, clase_modelo: Union[type[KNeighborsRegressor],
    type[LinearRegression], type[Ridge], type[Lasso]],
    params: Dict[str, Any] = None) -> Union[KNeighborsRegressor,
        LinearRegression, Ridge, Lasso]:
    """
    Entrenar un modelo utilizando la clase y los parámetros proporcionados.

    Args:
    - x (pd.Series): Variables independientes.
    - y (pd.Series): Variable dependiente.
    - clase_modelo (Union[type[KNeighborsRegressor], type[LinearRegression],
    type[Ridge], type[Lasso]]): Clase de modelo a entrenar.
    - params (Dict[str, Any], opcional): Parámetros del modelo.

    Returns:
    - Union[KNeighborsRegressor, LinearRegression, Ridge, Lasso]: Modelo
    entrenado.
    """
    print(f"Usando {clase_modelo.__name__} con {params}")
    print(f"¿Parámetros? {params is None or len(params)==0}")
    if params is None or len(params) == 0:
        modelo = clase_modelo()
    else:
        modelo = clase_modelo(**params)
    modelo_entrenado = modelo.fit(x, y)
    return modelo_entrenado

def crear_grafica(subgraficas: int = 111) -> Tuple[Figure, Axes]:
    """
    Crear una figura y un eje para graficar.

    Args:
    - subgraficas (int): Configuración de subgráficas de Matplotlib.

    Returns:
    - Tuple[Figure, Axes]: Figura y ejes creados.
    """
    figura = plt.figure()
    eje = figura.add_subplot(subgraficas)
    return figura, eje

def graficar_puntos(eje: Axes, x, y, params: Dict[str, Any]):
    """
    Graficar puntos en un eje dado.

    Args:
    - eje (Axes): Eje de Matplotlib donde se graficarán los puntos.
    - x: Coordenadas x de los puntos.
    - y: Coordenadas y de los puntos.
    - params (Dict[str, Any]): Parámetros para la función  scatter de
    Matplotlib.

    Returns:
    - PathCollection: Objeto de gráfica de dispersión de Matplotlib.
    """
    return eje.scatter(x, y, **params)

def graficar_linea(eje: Axes, x, y, color: str, etiqueta: str = 'linea'):
    """
    Graficar una línea en un eje dado.

    Args:
    - eje (Axes): Eje de Matplotlib donde se graficará la línea.
    - x: Coordenadas x de la línea.
    - y: Coordenadas y de la línea.
    - color (str): Color de la línea.
    - etiqueta (str): Etiqueta de la línea.

    Returns:
    - List[Line2D]: Lista de objetos de Matplotlib resultantes de la gráfica de
    la línea.
    """
    return eje.plot(x, y, color=color, label=etiqueta)

def configurar_y_guardar(fig: Figure, ejes: Axes, nombre_archivo: str,
    opciones: Dict[str, Any] = None) -> None:
    """
    Ajustar los parámetros del gráfico y guardar la figura en un archivo.

    Args:
    - fig (Figure): Figura de Matplotlib.
    - eje (Axes): Eje de Matplotlib.
    - nombre_archivo (str): Nombre del archivo donde se guardará la figura.
    - opciones (Dict[str, Any], opcional): Opciones adicionales para la figura
    (por ejemplo, límites, rotación y tamaño)
    """
    opciones = opciones or {}
    rotacion = opciones.get('rotacion', 90)
    tamano_pulgadas = opciones.get('tamano_pulgadas', (27, 18))
    limites_y = opciones.get('limites_y')

    ejes.tick_params(axis='x', labelrotation=rotacion)
    fig.set_size_inches(*tamano_pulgadas)
    if limites_y is not None:
        ejes.set_ylim(limites_y)
    fig.savefig(nombre_archivo)
    plt.close()

def preparar_archivo() -> pd.DataFrame:
    """
    Preparar un DataFrame con datos agrupados y transformados, provenientes de
    un archivo CSV.

    Returns:
    - pd.DataFrame: DataFrame preparado con datos agrupados y transformados.
    """
    # Cargar datos desde el archivo CSV
    df = pd.read_csv("csv/typed_uanl.csv")

    # Mostrar las primeras 10 filas del DataFrame
    imprimir_tabla(df.head(10))

    # Obtener sueldo total por fecha
    df_por_salario = df.groupby(["Tipo", "Fecha"])[["Sueldo Neto"]].agg(
        sueldo_neto_sum=('Sueldo Neto', 'sum'),
        conteo_empleados=('Sueldo Neto', 'count'),
        promedio_pago=('Sueldo Neto', 'mean')
    ).reset_index()

    return df_por_salario
